FROM quay.io/buildah/stable:v1.28.0

RUN dnf -y update && \
    dnf -y install qemu-user-static && \
    dnf -y clean all && \
    rm -rf /var/cache /var/log/dnf* /var/log/yum.*
